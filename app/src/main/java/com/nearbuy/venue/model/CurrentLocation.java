package com.nearbuy.venue.model;

public class CurrentLocation {

    private double latitude;
    private double longitude;

    public CurrentLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return String.valueOf(latitude) + "," + String.valueOf(longitude);
    }
}
