package com.nearbuy.venue.model.venue;

import java.util.ArrayList;

public class VenueListResponse {

    ArrayList<VenueDetails> venues;

    boolean confident;

    public ArrayList<VenueDetails> getVenues() {
        return venues;
    }

    public boolean isConfident() {
        return confident;
    }

}
