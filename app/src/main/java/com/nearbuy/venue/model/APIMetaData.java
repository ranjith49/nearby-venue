package com.nearbuy.venue.model;

public class APIMetaData {

    int code;
    String requestId;

    public int getCode() {
        return code;
    }

    public String getRequestId() {
        return requestId;
    }
}
