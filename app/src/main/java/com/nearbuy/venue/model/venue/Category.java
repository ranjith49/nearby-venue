package com.nearbuy.venue.model.venue;

public class Category {

    String id;
    String name;
    String pluralName;
    String shortName;
    boolean primary;
    CategoryIcon icon;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPluralName() {
        return pluralName;
    }

    public String getShortName() {
        return shortName;
    }

    public boolean isPrimary() {
        return primary;
    }

    public CategoryIcon getIcon() {
        return icon;
    }

}
