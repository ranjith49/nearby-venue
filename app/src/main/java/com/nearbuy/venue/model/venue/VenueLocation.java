package com.nearbuy.venue.model.venue;

public class VenueLocation {

    String address;
    String crossStreet;
    double lat;
    double lng;
    int distance;

    String city;
    String state;
    String country;
    String [] formattedAddress;

    public String getAddress() {
        return address;
    }

    public String getCrossStreet() {
        return crossStreet;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public int getDistance() {
        return distance;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getCountry() {
        return country;
    }

    public String[] getFormattedAddress() {
        return formattedAddress;
    }
}
