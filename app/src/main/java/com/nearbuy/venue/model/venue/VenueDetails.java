package com.nearbuy.venue.model.venue;

import java.util.ArrayList;

public class VenueDetails {

    String id;
    String name;
    VenueLocation location;
    ArrayList<Category> categories;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public VenueLocation getLocation() {
        return location;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }
}
