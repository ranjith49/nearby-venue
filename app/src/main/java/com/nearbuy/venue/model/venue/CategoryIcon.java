package com.nearbuy.venue.model.venue;

public class CategoryIcon {

    String prefix;
    String suffix;

    public String getPrefix() {
        return prefix;
    }

    public String getSuffix() {
        return suffix;
    }
}
