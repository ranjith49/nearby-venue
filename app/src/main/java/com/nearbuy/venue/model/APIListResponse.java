package com.nearbuy.venue.model;

import java.util.List;

/**
 * Generic API response , which has meta data and as well as List<Response> objects
 *
 * @param <META>     -- meta data object
 * @param <RESPONSE> -- response object
 */
public class APIListResponse<META, RESPONSE> {

    META meta;

    RESPONSE response;

    public META getMeta() {
        return meta;
    }

    public RESPONSE getResponse() {
        return response;
    }

}
