package com.nearbuy.venue.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;

import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.model.CurrentLocation;
import com.nearbuy.venue.model.venue.Category;
import com.nearbuy.venue.model.venue.CategoryIcon;
import com.nearbuy.venue.model.venue.VenueDetails;
import com.nearbuy.venue.service.NearByVenueService;
import com.nearbuy.venue.service.NearByVenueServiceImpl;
import com.nearbuy.venue.view.VenueInfoUtils;
import com.nearbuy.venue.viewmodel.model.CategoryInfo;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Presenter for View model , to load the nearby venues
 *
 * @author ranjithsuda
 */
public class NearByVenuePresenter implements NearByVenueService.Listener {

    private static final String TAG = NearByVenuePresenter.class.getSimpleName();
    private NearByVenueView nearByVenueView;
    private NearByVenueService nearByVenueService;
    private transient AtomicBoolean dataLoading;

    public NearByVenuePresenter(NearByVenueView nearByVenueView) {
        this.nearByVenueView = nearByVenueView;
        this.dataLoading = new AtomicBoolean();

        this.nearByVenueService = new NearByVenueServiceImpl(this);
    }

    public void fetchNearBuyVenues(double latitute, double longitude) {
        if (dataLoading.get()) {
            Log.d(TAG, "Request in progress");
            return;
        }

        dataLoading.set(true);

        CurrentLocation currentLocation = new CurrentLocation(latitute, longitude);
        nearByVenueService.getNearByVenues(currentLocation);
    }

    public void cancelNwReqIfAny() {
        if (nearByVenueService != null) {
            nearByVenueService.cancelNearByReq();
        }
    }

    @Override
    public void onNearByVenuesFetched(@NonNull List<VenueDetails> venuesList) {
        ArrayList<VenueInfo> venueInfoList = new ArrayList<>();
        for (VenueDetails venueDetail : venuesList) {

            String locationData = null;
            int distance = 0;
            Pair<Double, Double> latLongPair = null;
            CategoryInfo categoryInfo = null;

            if (venueDetail.getLocation() != null) {
                locationData = VenueInfoUtils.getLocationInfoData(venueDetail.getLocation());
                distance = venueDetail.getLocation().getDistance();
                latLongPair = new Pair<>(venueDetail.getLocation().getLat(), venueDetail.getLocation().getLng());
            }

            // Fetch Primary category ..
            if (venueDetail.getCategories() != null) {
                for (Category category : venueDetail.getCategories()) {
                    if (!category.isPrimary()) {
                        Log.d(TAG, "This is not primary");
                        continue;
                    }

                    CategoryIcon icon = category.getIcon();
                    categoryInfo = new CategoryInfo.Builder(category.getId(), category.getName())
                            .setUrl(VenueInfoUtils.getCategoryUrl(icon))
                            .build();

                }
            }

            // Build Venue Info ..
            VenueInfo venueInfo = new VenueInfo.Builder(venueDetail.getId(), venueDetail.getName())
                    .setLocationData(TextUtils.isEmpty(locationData) ? NearByConstants.EMPTY_STRING : locationData)
                    .setLatLong(latLongPair == null ? VenueInfoUtils.getInvalidLatLongPair() : latLongPair)
                    .setCatInfo(categoryInfo)
                    .setDistance(distance)
                    .build();

            venueInfoList.add(venueInfo);
        }

        dataLoading.set(false);

        if (nearByVenueView != null) {
            nearByVenueView.onNearByVenuesLoaded(venueInfoList);
        }
    }

    @Override
    public void onNoVenuesFetched(@NearByConstants.DataErrorCodes int dataCode) {
        dataLoading.set(false);
        if (nearByVenueView != null) {
            nearByVenueView.onNearByVenuesError(dataCode);
        }
    }
}
