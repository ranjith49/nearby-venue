package com.nearbuy.venue.presenter;

import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.ArrayList;

public interface NearByVenueView {

    /**
     * Call back when a list of near by venues are loaded
     *
     * @param venuInfoList -- list of venues
     */
    public void onNearByVenuesLoaded(ArrayList<VenueInfo> venuInfoList);

    /**
     * Call back on near by venues error
     *
     * @param dataCode -- data code correspondingly
     */
    public void onNearByVenuesError(@NearByConstants.DataErrorCodes int dataCode);
}
