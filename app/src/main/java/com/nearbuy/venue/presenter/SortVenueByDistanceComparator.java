package com.nearbuy.venue.presenter;

import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.Comparator;

public class SortVenueByDistanceComparator implements Comparator<VenueInfo> {

    public SortVenueByDistanceComparator() {

    }

    @Override
    public int compare(VenueInfo v1, VenueInfo v2) {
        if (v1.getDistance() == -1 || v2.getDistance() == -1) {     // Keep all not known locations to END
            return 1;
        }

        if (v1.getDistance() == v2.getDistance()) {
            return 0;
        } else if (v1.getDistance() < v2.getDistance()) {
            return -1;
        } else {
            return 1;
        }
    }
}
