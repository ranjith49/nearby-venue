package com.nearbuy.venue.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.location.Location;
import android.util.Log;

import com.nearbuy.venue.presenter.NearByVenuePresenter;
import com.nearbuy.venue.presenter.NearByVenueView;
import com.nearbuy.venue.presenter.SortVenueByDistanceComparator;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.ArrayList;
import java.util.Collections;

public class NearByVenuesModel extends ViewModel implements NearByVenueView {

    private static final String TAG = NearByVenuesModel.class.getSimpleName();
    private ArrayList<VenueInfo> venueInfoList; // Current Venue Info List ..

    private MutableLiveData<Boolean> venueLoadProgessBar;
    private MutableLiveData<ArrayList<VenueInfo>> venueListLiveData;
    private MutableLiveData<Integer> venueLoadError;

    private NearByVenuePresenter nearByVenuePresenter;

    public NearByVenuesModel() {
        this.venueListLiveData = new MutableLiveData<>();
        this.venueLoadError = new MutableLiveData<>();
        this.venueLoadProgessBar = new MutableLiveData<>();

        this.nearByVenuePresenter = new NearByVenuePresenter(this);
    }

    public void fetchVenueInfoList(Location location) {
        this.venueInfoList = null;

        venueLoadProgessBar.postValue(true);
        nearByVenuePresenter.fetchNearBuyVenues(location.getLatitude(), location.getLongitude());
    }

    /**
     * Sort list based on option provider
     */
    public void sortList() {
        Collections.sort(venueInfoList, new SortVenueByDistanceComparator());
        venueListLiveData.postValue(venueInfoList);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (nearByVenuePresenter != null) {
            nearByVenuePresenter.cancelNwReqIfAny();
        }
    }

    @Override
    public void onNearByVenuesLoaded(ArrayList<VenueInfo> venuInfoList) {
        Log.d(TAG, "On List loaded");

        this.venueInfoList = venuInfoList;

        venueLoadProgessBar.postValue(false);
        venueListLiveData.postValue(venuInfoList);
    }

    @Override
    public void onNearByVenuesError(int dataCode) {
        venueLoadProgessBar.postValue(false);
        venueLoadError.postValue(dataCode);
    }

    public MutableLiveData<Boolean> getVenueLoadProgessBar() {
        return venueLoadProgessBar;
    }

    public MutableLiveData<ArrayList<VenueInfo>> getVenueListLiveData() {
        return venueListLiveData;
    }

    public MutableLiveData<Integer> getVenueLoadError() {
        return venueLoadError;
    }

    public ArrayList<VenueInfo> getVenueInfoList() {
        return venueInfoList;
    }
}
