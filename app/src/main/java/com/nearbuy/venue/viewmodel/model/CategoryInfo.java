package com.nearbuy.venue.viewmodel.model;

import com.nearbuy.venue.NearByConstants;

/**
 * Model required for showing category in a venue
 *
 * @author ranjithsuda
 */
public class CategoryInfo {

    private String categoryId;
    private String categoryName;
    private String categoryIconUrl;

    private CategoryInfo(Builder builder) {
        this.categoryId = builder.id;
        this.categoryName = builder.name;
        this.categoryIconUrl = builder.url;
    }

    public static class Builder {
        private String id;
        private String name;

        // Optional ..
        private String url = NearByConstants.EMPTY_STRING;

        public Builder(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder setUrl(String url) {
            this.url = url;
            return this;
        }

        public CategoryInfo build() {
            return new CategoryInfo(this);
        }
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getCategoryIconUrl() {
        return categoryIconUrl;
    }

}
