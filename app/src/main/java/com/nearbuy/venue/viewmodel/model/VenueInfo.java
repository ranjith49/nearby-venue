package com.nearbuy.venue.viewmodel.model;

import android.support.annotation.NonNull;
import android.util.Pair;

import com.nearbuy.venue.NearByConstants;

public class VenueInfo {

    private String venueId;
    private String venueName;
    private String displayLocationData;
    private int distance;

    private Pair<Double, Double> latlongPair;
    private CategoryInfo categoryInfo;


    private VenueInfo(Builder builder) {
        this.venueId = builder.id;
        this.venueName = builder.name;
        this.displayLocationData = builder.locationInfo;
        this.distance = builder.distance;
        this.latlongPair = builder.latLongPair;
        this.categoryInfo = builder.info;
    }

    public static class Builder {

        private String id;
        private String name;

        // Optional ..
        private String locationInfo = NearByConstants.EMPTY_STRING;
        private int distance = -1;
        private Pair<Double, Double> latLongPair = null;
        private CategoryInfo info = null;

        public Builder(String id, String name) {
            this.id = id;
            this.name = name;
        }

        public Builder setLocationData(@NonNull String data) {
            this.locationInfo = data;
            return this;
        }

        public Builder setDistance(int distance) {
            this.distance = distance;
            return this;
        }

        public Builder setLatLong(@NonNull Pair<Double, Double> latLong) {
            this.latLongPair = latLong;
            return this;
        }

        public Builder setCatInfo(CategoryInfo catInfo) {
            this.info = catInfo;
            return this;
        }

        public VenueInfo build() {
            return new VenueInfo(this);
        }

    }

    public String getVenueId() {
        return venueId;
    }

    public String getVenueName() {
        return venueName;
    }

    public String getDisplayLocationData() {
        return displayLocationData;
    }

    public int getDistance() {
        return distance;
    }

    public Pair<Double, Double> getLatlongPair() {
        return latlongPair;
    }

    public CategoryInfo getCategoryInfo() {
        return categoryInfo;
    }
}
