package com.nearbuy.venue.view;

import com.nearbuy.venue.viewmodel.model.VenueInfo;

public interface NearByInfoUpdate {

    /**
     * On near by info updated
     *
     * @param info -- info
     */
    public void onNearByInfoUpdate(VenueInfo info);
}
