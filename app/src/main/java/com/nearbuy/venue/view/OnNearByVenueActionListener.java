package com.nearbuy.venue.view;

import com.nearbuy.venue.viewmodel.model.VenueInfo;

public interface OnNearByVenueActionListener {

    /**
     * Click listener on near by venue
     *
     * @param info -- info
     */
    public void onNearByVenueActionClick(VenueInfo info);
}
