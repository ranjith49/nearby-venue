package com.nearbuy.venue.view.viewholder;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.nearbuy.venue.NearByApplication;
import com.nearbuy.venue.R;
import com.nearbuy.venue.view.NearByInfoUpdate;
import com.nearbuy.venue.view.OnNearByVenueActionListener;
import com.nearbuy.venue.view.VenueInfoUtils;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

public class NearByLocationViewHolder extends RecyclerView.ViewHolder implements NearByInfoUpdate {

    private RelativeLayout venueParent;
    private TextView locationName;
    private TextView locationAddress;
    private SimpleDraweeView categoryIcon;
    private TextView locationDistance;
    private TextView categoryName;

    private OnNearByVenueActionListener listener;

    public NearByLocationViewHolder(View itemView, OnNearByVenueActionListener listener) {
        super(itemView);

        this.listener = listener;
        venueParent = itemView.findViewById(R.id.venue_parent);
        locationName = itemView.findViewById(R.id.venue_name);
        locationDistance = itemView.findViewById(R.id.venue_distance);
        locationAddress = itemView.findViewById(R.id.venue_address);
        categoryIcon = itemView.findViewById(R.id.categoryIcon);
        categoryName = itemView.findViewById(R.id.categoryName);
    }

    @Override
    public void onNearByInfoUpdate(final VenueInfo info) {
        locationName.setText(info.getVenueName());
        if (info.getDistance() != -1) {
            locationDistance.setText(NearByApplication.getAppContext().getResources().getString(R.string.venue_distance_meters, info.getDistance()));
            locationDistance.setVisibility(View.VISIBLE);
        } else {
            locationDistance.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(info.getDisplayLocationData())) {
            locationAddress.setText(info.getDisplayLocationData());
            locationAddress.setVisibility(View.VISIBLE);
        } else {
            locationAddress.setVisibility(View.GONE);
        }

        if (info.getCategoryInfo() != null) {
            categoryName.setText(info.getCategoryInfo().getCategoryName());
            categoryName.setVisibility(View.VISIBLE);

            Uri uri = Uri.parse(info.getCategoryInfo().getCategoryIconUrl());
            categoryIcon.setImageURI(uri);
        } else {
            categoryIcon.setImageResource(R.drawable.location_on_white);
            categoryName.setVisibility(View.GONE);
        }

        venueParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNearByVenueActionClick(info);
            }
        });
    }
}
