package com.nearbuy.venue.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nearbuy.venue.R;
import com.nearbuy.venue.view.OnNearByVenueActionListener;
import com.nearbuy.venue.view.viewholder.NearByLocationViewHolder;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.ArrayList;

/**
 * Adapter for Near by location
 *
 * @author ranjithsuda
 */
public class NearByLocationAdapter extends RecyclerView.Adapter<NearByLocationViewHolder> {

    private ArrayList<VenueInfo> venueInfoList;
    private OnNearByVenueActionListener actionListener;

    public NearByLocationAdapter(ArrayList<VenueInfo> list, OnNearByVenueActionListener actionListener) {
        this.actionListener = actionListener;
        this.venueInfoList = list;
    }

    @NonNull
    @Override
    public NearByLocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.venue_info_item, parent, false);
        return new NearByLocationViewHolder(view,actionListener);
    }

    @Override
    public void onBindViewHolder(@NonNull NearByLocationViewHolder holder, int position) {
        holder.onNearByInfoUpdate(venueInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return venueInfoList.size();
    }
}
