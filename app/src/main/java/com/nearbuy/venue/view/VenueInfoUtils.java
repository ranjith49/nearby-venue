package com.nearbuy.venue.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.nearbuy.venue.NearByApplication;
import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.model.venue.CategoryIcon;
import com.nearbuy.venue.model.venue.VenueLocation;

import java.util.Arrays;

/**
 * Helper class for generation utils
 *
 * @author ranjithsuda
 */
public class VenueInfoUtils {

    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int CATEGORY_ICON_SIZE = 32;

    public static String getLocationInfoData(VenueLocation location) {
        if (location == null) {
            return NearByConstants.EMPTY_STRING;
        }

        if (location.getFormattedAddress() != null) {
            return Arrays.toString(location.getFormattedAddress());
        }

        StringBuilder locationData = new StringBuilder();
        if (!TextUtils.isEmpty(location.getAddress())) {
            locationData.append(location.getAddress());
            locationData.append(", ");
        }
        if (!TextUtils.isEmpty(location.getCity())) {
            locationData.append(location.getCity());
            locationData.append(", ");
        }
        if (!TextUtils.isEmpty(location.getState())) {
            locationData.append(location.getState());
            locationData.append(", ");
        }
        return locationData.toString();
    }

    public static String getCategoryUrl(CategoryIcon icon) {
        if (icon == null)
            return NearByConstants.EMPTY_STRING;

        StringBuilder iconUrl = new StringBuilder();
        iconUrl.append(icon.getPrefix());
        iconUrl.append(CATEGORY_ICON_SIZE);
        iconUrl.append(icon.getSuffix());
        return iconUrl.toString();
    }

    public static Pair<Double, Double> getInvalidLatLongPair() {
        return new Pair<>(0.0d, 0.0d);
    }

    public static int getSpanCount(Context activityContext) {
        if (activityContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return 3;
        } else {
            return 2;
        }
    }
}
