package com.nearbuy.venue.view.activity;

import android.Manifest;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.SettingsClient;
import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.R;
import com.nearbuy.venue.view.OnNearByVenueActionListener;
import com.nearbuy.venue.view.VenueInfoUtils;
import com.nearbuy.venue.view.adapter.NearByLocationAdapter;
import com.nearbuy.venue.viewmodel.MyLocationViewModel;
import com.nearbuy.venue.viewmodel.NearByVenuesModel;
import com.nearbuy.venue.viewmodel.model.VenueInfo;

import java.util.ArrayList;

/**
 * Near By Locations List
 *
 * @author ranjithsuda
 */
public class NearByLocationListActivity extends AppCompatActivity implements OnNearByVenueActionListener {

    private static final String TAG = NearByLocationListActivity.class.getSimpleName();
    private final int REQUEST_CHECK_SETTINGS = 2000;

    private RelativeLayout parentLayout;
    private RecyclerView nearByLocationList;
    private Button sortByDistance;
    private FloatingActionButton fetchCurrentLocation;

    private RelativeLayout errorLayout;
    private TextView infoReason;
    private Button infoRetry;

    private ProgressBar loadProgressBar;
    private NearByLocationAdapter locationAdapter;

    private NearByVenuesModel venuesModel;
    private MyLocationViewModel locationViewModel;

    private static final int REQUEST_LOCATION = 0;

    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;


    @Override
    protected void onCreate(@Nullable Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.near_by_location);
        initView();
        initLocationViewModel();
        initNearbyViewModel();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        locationViewModel.init(mFusedLocationClient, mSettingsClient);

        if (venuesModel != null && venuesModel.getVenueInfoList() != null) {
            showNearByVenueList(venuesModel.getVenueInfoList());
        } else {
            showInfoState(NearByConstants.DataErrorCodes.EMPTY_FIRST_TIME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isLocationPermissionGranted()) {
            locationViewModel.startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isLocationPermissionGranted()) {
            locationViewModel.stopLocationUpdates();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initView() {
        parentLayout = findViewById(R.id.parentLayout);
        nearByLocationList = findViewById(R.id.nearbyLocationList);
        errorLayout = findViewById(R.id.info_layout);
        infoReason = findViewById(R.id.info_reason_text);
        infoRetry = findViewById(R.id.info_location_fetch);
        loadProgressBar = findViewById(R.id.loading_progress);

        fetchCurrentLocation = findViewById(R.id.fetchMyLocation);
        fetchCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchVenuListForALocation();
            }
        });

        sortByDistance = findViewById(R.id.sortByDistance);
        sortByDistance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                venuesModel.sortList();
            }
        });
    }

    private void fetchVenuListForALocation() {
        if (isLocationPermissionGranted()) {
            Location currentLocation = locationViewModel.getCurrentMyLocation();
            if (currentLocation == null) {
                showInfoState(NearByConstants.DataErrorCodes.DATA_REQ_LOCATION_FAILED);
            } else {
                // If any ..
                errorLayout.setVisibility(View.GONE);
                nearByLocationList.setVisibility(View.GONE);

                venuesModel.fetchVenueInfoList(currentLocation);
            }
        } else {
            requestLocationPermission();
        }
    }

    private void initLocationViewModel() {
        locationViewModel = ViewModelProviders.of(this).get(MyLocationViewModel.class);
        locationViewModel.getLocationStatusResolutionReq().observe(this, new Observer<ResolvableApiException>() {
            @Override
            public void onChanged(@Nullable ResolvableApiException rae) {
                if (rae == null) {
                    return;
                }
                try {
                    rae.startResolutionForResult(NearByLocationListActivity.this, REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException sie) {
                    Log.i(TAG, "PendingIntent unable to execute request.");
                }
            }
        });

    }

    private void initNearbyViewModel() {
        venuesModel = ViewModelProviders.of(this).get(NearByVenuesModel.class);
        venuesModel.getVenueLoadError().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer state) {
                if (state == null) {
                    return;
                }
                showInfoState(state);
            }
        });
        venuesModel.getVenueLoadProgessBar().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean showOrHide) {
                if (showOrHide == null) {
                    return;
                }
                showOrHideProgressBar(showOrHide);
            }
        });
        venuesModel.getVenueListLiveData().observe(this, new Observer<ArrayList<VenueInfo>>() {
            @Override
            public void onChanged(@Nullable ArrayList<VenueInfo> list) {
                if (list == null) {
                    return;
                }
                showNearByVenueList(list);
            }
        });
    }

    /**
     * Helper method to show or hide Progress Bar
     *
     * @param showOrHide -- show or hide progress bar
     */
    private void showOrHideProgressBar(boolean showOrHide) {
        if (showOrHide) {
            loadProgressBar.setVisibility(View.VISIBLE);
        } else {
            loadProgressBar.setVisibility(View.GONE);
        }
    }

    private void showNearByVenueList(ArrayList<VenueInfo> venueList) {
        locationAdapter = new NearByLocationAdapter(venueList, this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, VenueInfoUtils.getSpanCount(this));

        nearByLocationList.setLayoutManager(layoutManager);
        nearByLocationList.setAdapter(locationAdapter);

        nearByLocationList.setVisibility(View.VISIBLE);
        sortByDistance.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
    }

    private void showInfoState(@NearByConstants.DataErrorCodes int dataCode) {
        switch (dataCode) {
            case NearByConstants.DataErrorCodes.EMPTY_FIRST_TIME:
                infoReason.setText(R.string.info_empty_first_time);
                infoRetry.setVisibility(View.GONE);
                break;
            case NearByConstants.DataErrorCodes.DATA_REQ_LOCATION_FAILED:
                infoReason.setText(R.string.error_not_able_get_location);
                infoRetry.setVisibility(View.VISIBLE);
                break;
            case NearByConstants.DataErrorCodes.EMPTY_DUE_TO_NW_CONNECTION:
                infoReason.setText(R.string.error_venue_load_fail_nw);
                infoRetry.setVisibility(View.VISIBLE);
                break;
            case NearByConstants.DataErrorCodes.DATA_FETCH_ERROR:
                infoReason.setText(R.string.error_venue_load_fail);
                infoRetry.setVisibility(View.VISIBLE);
                break;
            case NearByConstants.DataErrorCodes.EMPTY_DATA:
                infoReason.setText(R.string.error_venue_load_empty);
                infoRetry.setVisibility(View.VISIBLE);
                break;
            case NearByConstants.DataErrorCodes.DATA_REQ_PERMISSION_NOT_GRANTED:
                infoReason.setText(R.string.error_no_location_permission);
                infoRetry.setVisibility(View.VISIBLE);
                break;
            case NearByConstants.DataErrorCodes.UNKNOWN:
            default:
                infoReason.setText(R.string.error_venue_load_fail_generic);
                infoRetry.setVisibility(View.VISIBLE);
                break;
        }

        infoRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetchVenuListForALocation();
            }
        });

        errorLayout.setVisibility(View.VISIBLE);
        nearByLocationList.setVisibility(View.GONE);
        sortByDistance.setVisibility(View.GONE);
    }

    @Override
    public void onNearByVenueActionClick(VenueInfo info) {
        if (info.getLatlongPair() == null) {
            return;
        }

        String gmsUri = "geo:" + info.getLatlongPair().first + "," + info.getLatlongPair().second;

        Uri gmmIntentUri = Uri.parse(gmsUri + "?q=" + info.getLatlongPair().first + "," + info.getLatlongPair().second+"("+info.getVenueName()+")");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void requestLocationPermission() {
        Log.i(TAG, "LOCATION permission has NOT been granted. Requesting permission.");
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            Log.i(TAG, "Displaying LOCATION permission rationale to provide additional context.");
            Snackbar.make(parentLayout, R.string.location_permission_fetch, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(NearByLocationListActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            Log.i(TAG, "Received response for location permission request.");

            // Check if the only required permission has been granted
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "LOCATION permission has now been granted. Showing preview.");
                showInfoState(NearByConstants.DataErrorCodes.EMPTY_FIRST_TIME);
            } else {
                Log.i(TAG, "LOCATION permission has not been granted. Showing preview.");
                Snackbar.make(parentLayout, R.string.location_permission_fetch_failed, Snackbar.LENGTH_SHORT).show();
                showInfoState(NearByConstants.DataErrorCodes.DATA_REQ_PERMISSION_NOT_GRANTED);
            }
        }
    }

    private boolean isLocationPermissionGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.d(TAG, "ON Settings success");
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                    default:
                        break;
                }
                break;
        }
    }
}
