package com.nearbuy.venue;

import android.support.annotation.IntDef;

public interface NearByConstants {

    String API_BASE_URL = "https://api.foursquare.com/v2/";

    String APP_CLIENT_ID = "QXH4CS30YDN0MZTOKIV424OJAI3JZ3LUSR4FNMML3D0JH02C";
    String APP_CLIENT_SECRET = "JAJ5EXIC4N2SO0JPSRSYWUWMSKATXVZHWRLJII25VHEY2YHY";

    String EMPTY_STRING = "";

    @IntDef({
            DataErrorCodes.DATA_FETCH_ERROR, DataErrorCodes.EMPTY_DATA,
            DataErrorCodes.EMPTY_DUE_TO_NW_CONNECTION, DataErrorCodes.UNKNOWN,
            DataErrorCodes.EMPTY_FIRST_TIME,
            DataErrorCodes.DATA_REQ_LOCATION_FAILED, DataErrorCodes.DATA_REQ_PERMISSION_NOT_GRANTED
    })
    public @interface DataErrorCodes {
        int UNKNOWN = 0;
        int EMPTY_FIRST_TIME = 1;
        int DATA_FETCH_ERROR = 2;
        int EMPTY_DATA = 3;
        int EMPTY_DUE_TO_NW_CONNECTION = 4;

        int DATA_REQ_LOCATION_FAILED = 5;
        int DATA_REQ_PERMISSION_NOT_GRANTED = 6;
    }
}
