package com.nearbuy.venue.service;

import android.support.annotation.NonNull;

import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.model.CurrentLocation;
import com.nearbuy.venue.model.venue.VenueDetails;

import java.util.List;

/**
 * Contract for Near by Venue API
 */
public interface NearByVenueService {

    /**
     * API call to get the near by venues for a given location
     *
     * @param location -- location object
     */
    public void getNearByVenues(CurrentLocation location);

    /**
     * Call this method to cancel near by request , if any
     */
    public void cancelNearByReq();


    public interface Listener {

        public void onNearByVenuesFetched(@NonNull List<VenueDetails> venuesList);

        public void onNoVenuesFetched(@NearByConstants.DataErrorCodes int dataCode);
    }
}
