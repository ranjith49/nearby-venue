package com.nearbuy.venue.service;

import com.nearbuy.venue.NearByConstants;
import com.nearbuy.venue.model.APIListResponse;
import com.nearbuy.venue.model.APIMetaData;
import com.nearbuy.venue.model.CurrentLocation;
import com.nearbuy.venue.model.venue.VenueDetails;
import com.nearbuy.venue.model.venue.VenueListResponse;
import com.nearbuy.venue.nw.NetworkConstants;
import com.nearbuy.venue.nw.NetworkUtils;
import com.nearbuy.venue.nw.SearchVenueAPI;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;

public class NearByVenueServiceImpl extends BaseNwService<APIListResponse<APIMetaData, VenueListResponse>> implements NearByVenueService {

    private NearByVenueService.Listener listener;
    private CurrentLocation currentLocation;

    public NearByVenueServiceImpl(NearByVenueService.Listener listener) {
        this.listener = listener;
    }

    @Override
    public Call<APIListResponse<APIMetaData, VenueListResponse>> createNwCallObject() {
        SearchVenueAPI searchVenueAPI = NetworkUtils.getRetrofit().create(SearchVenueAPI.class);

        Map<String, String> queryParams = new HashMap<String, String>();
        queryParams.put(NetworkConstants.CLIENT_ID_KEY, NearByConstants.APP_CLIENT_ID);
        queryParams.put(NetworkConstants.CLIENT_SECRET_KEY, NearByConstants.APP_CLIENT_SECRET);
        queryParams.put(NetworkConstants.LOCATION_KEY, currentLocation.toString());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.UK);
        String yyyyMMdd = sdf.format(new Date());

        queryParams.put(NetworkConstants.VERSION_KEY, yyyyMMdd);

        return searchVenueAPI.getNearByVenues(queryParams);
    }

    @Override
    public void onSuccessResponse(APIListResponse<APIMetaData, VenueListResponse> response) {
        if (response == null || response.getMeta() == null || response.getMeta().getCode() != NetworkConstants.HttpCodes.REQUEST_OK) {
            listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.DATA_FETCH_ERROR);
            return;
        }

        if (response.getResponse() == null || response.getResponse().getVenues() == null) {
            listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.EMPTY_DATA);
        }

        listener.onNearByVenuesFetched(response.getResponse().getVenues());
    }

    @Override
    public void onError(@NetworkConstants.HttpCodes int errorCode) {
        switch (errorCode) {
            case NetworkConstants.HttpCodes.REQUEST_NOT_ACCEPTABLE:
                listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.DATA_FETCH_ERROR);
                break;
            case NetworkConstants.HttpCodes.SERVER_ERROR:
                listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.DATA_FETCH_ERROR);
                break;
            case NetworkConstants.HttpCodes.SERVICE_UNAVAILABLE:
                listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.EMPTY_DUE_TO_NW_CONNECTION);
                break;
            case NetworkConstants.HttpCodes.REQUEST_OK:
            default:
                listener.onNoVenuesFetched(NearByConstants.DataErrorCodes.UNKNOWN);
                break;
        }
    }

    @Override
    public void getNearByVenues(CurrentLocation location) {
        this.currentLocation = location;
        makeNetworkCall();
    }

    @Override
    public void cancelNearByReq() {
        cancelCallReq();
    }
}
