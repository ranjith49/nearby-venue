package com.nearbuy.venue.service;

import android.support.annotation.NonNull;

import com.nearbuy.venue.NearByApplication;
import com.nearbuy.venue.nw.NetworkConstants;
import com.nearbuy.venue.nw.NetworkUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Generic Network layer service that makes n/w request and provides the T data model back
 *
 * @param <T> -- Data Model , which caller is expecting to return
 */
public abstract class BaseNwService<T> {

    private Call<T> nwCall;

    public BaseNwService() {
        // Nothing to do ..
    }

    /**
     * Helper method to decide and make n/w call
     */
    public void makeNetworkCall() {
        boolean isNwConnected = NetworkUtils.isInternetAvailableOrNot(NearByApplication.getAppContext());
        if (!isNwConnected) {
            onError(NetworkConstants.HttpCodes.SERVICE_UNAVAILABLE);
            return;
        }

        // Actual call ..
        nwCall = createNwCallObject();
        nwCall.enqueue(new Callback<T>() {
            @Override
            public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {
                if (response.isSuccessful()) {
                    onSuccessResponse(response.body());
                } else {
                    onError(NetworkConstants.HttpCodes.REQUEST_NOT_ACCEPTABLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
                onError(NetworkConstants.HttpCodes.SERVER_ERROR);
            }
        });
    }

    /**
     * Helper method to cancel a n/w call request . if any in progress
     */
    public void cancelCallReq() {
        if (nwCall != null) {
            nwCall.cancel();
        }
    }

    /**
     * Method to get the call  Object to make a n/w req ..
     *
     * @return -- Call object created ..
     */
    public abstract Call<T> createNwCallObject();

    /**
     * Method to return on success response
     *
     * @param response -- response
     */
    public abstract void onSuccessResponse(T response);

    /**
     * Method to return on Error
     *
     * @param errorCode
     */
    public abstract void onError(@NetworkConstants.HttpCodes int errorCode);
}
