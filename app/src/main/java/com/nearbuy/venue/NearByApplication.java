package com.nearbuy.venue;

import android.app.Application;
import android.content.Context;

import com.facebook.drawee.backends.pipeline.Fresco;

public class NearByApplication extends Application {

    private static Context applicationContext;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getApplicationContext();
        Fresco.initialize(this);
    }

    public static Context getAppContext() {
        return applicationContext;
    }
}
