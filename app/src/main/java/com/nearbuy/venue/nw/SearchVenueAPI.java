package com.nearbuy.venue.nw;


import com.nearbuy.venue.model.APIMetaData;
import com.nearbuy.venue.model.APIListResponse;
import com.nearbuy.venue.model.venue.VenueDetails;
import com.nearbuy.venue.model.venue.VenueListResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface SearchVenueAPI {


    /**
     * API request to get the List of venues near by a location [lat and long
     *
     * @param queryOptions -- query options include clientId, client secret and ll [default intent = checkin assumed]
     *
     * @return -- list of Venues matching given location [lat and long]
     */
    @GET("venues/search")
    Call<APIListResponse<APIMetaData, VenueListResponse>> getNearByVenues(@QueryMap Map<String, String> queryOptions);
}
