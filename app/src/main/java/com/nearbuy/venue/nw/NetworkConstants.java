package com.nearbuy.venue.nw;

import android.support.annotation.IntDef;

public interface NetworkConstants {

    String CLIENT_ID_KEY = "client_id";
    String CLIENT_SECRET_KEY = "client_secret";
    String LOCATION_KEY = "ll";
    String VERSION_KEY ="v";

    @IntDef({
            HttpCodes.REQUEST_OK, HttpCodes.SERVICE_UNAVAILABLE,
            HttpCodes.REQUEST_NOT_ACCEPTABLE, HttpCodes.SERVER_ERROR
    })
    public @interface HttpCodes {
        int REQUEST_OK = 200;
        int SERVER_ERROR = 500;
        int SERVICE_UNAVAILABLE = 503;
        int REQUEST_NOT_ACCEPTABLE = 406;
    }
}
